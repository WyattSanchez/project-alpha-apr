from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required

# from tasks.models import Task
from projects.forms import ProjectForm


@login_required
def list_projects(request):
    showprojects = Project.objects.filter(owner=request.user)
    context = {"list": showprojects}
    return render(request, "projects/list.html", context)


@login_required
def show_project(request, id):
    thisproject = get_object_or_404(Project, id=id)
    context = {"project_object": thisproject}
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            catagory = form.save(commit=False)
            catagory.owner = request.user
            catagory.save()
            return redirect(list_projects)
    else:
        form = ProjectForm()
    context = {
        "post_form": form,
    }
    return render(request, "projects/create.html", context)
