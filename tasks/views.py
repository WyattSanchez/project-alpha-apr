from django.shortcuts import render, redirect
from tasks.forms import TaskForm
from projects.views import list_projects
from django.contrib.auth.decorators import login_required
from tasks.models import Task


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            task = form.save()
            task.owner = request.user
            task.save()
            return redirect(list_projects)
    else:
        form = TaskForm()
    context = {
        "post_form": form,
    }
    return render(request, "tasks/create_tasks.html", context)


@login_required
def show_my_tasks(request):
    if request.user.is_anonymous:
        return redirect("login")
    showmytasks = Task.objects.filter(assignee=request.user)
    context = {"my_tasks": showmytasks}
    return render(request, "tasks/show_my_tasks.html", context)
